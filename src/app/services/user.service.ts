import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from '../models/user';

@Injectable({ providedIn: 'root' })
export class UserService {

    private API_URL = 'http://localhost:8080';

    constructor(private http: HttpClient) { }

    register(user: User) {
        return this.http.post(this.API_URL + '/auth/register', user);
    }

}