export class User {
    id: number;
    ime: string;
    prezime: string;
    email: string;
    sifra: string;
    token: string;
}